# Micro Challenge II MDEF - Planetary Soda

## **Initial Idea/Concept of the Project**

### Before we created the Planetary Soda idea/concept, we began by learning and researching more about overlooked global problems & challenges on the topic of food, health & nutrition. We started going through content on World Health Organization, Food and Agriculture Organization and, eventually, the Global Nutrition Report. There, we found something really interesting and bizarre: of all the school-aged children in the world, _about a third (30.3%) of do not eat any fruit daily, yet 43.7% consume soda every day..._ Therefore, Planetary Soda consists of a critique of the current state of malnutrition and illness in the world. Our intervention aims to further highlight the ridiculousness and unacceptability of this crisis. 

## **How We Planned and Executed the Project** 

### Brainstorming Process
![Ideation Process](Pictures/Ideation_Process.png)

### Microgreens Acquisition & Growth Process

![Microgreens Growth Process 1](Pictures/Microgreens_Growing_Process_2.jpeg)

![Microgreens Growth Process 2](Pictures/Microgreens_Growing_Process_3.jpeg)

![Microgreens Growth Process 3](Pictures/Microgreens_Growing_Process.jpeg)

#### We acquired our microgreens kits at InstaGreen, an indoor urban farming company just a couple blocks from IAAC. They are very easy to grow, as observed in the pictures above. You just need to insert the cellulose mat on the small carboard tray, add the seeds, put more or less half a cup and water, close the lid, and let it germinate for 2-3 days. After that period, open the lid to let the sunlight in and keep irrigating every 2 days. 

## Logo

![Planetary Soda Logo](Pictures/LOGO_COMPLETED.png)

## **System Diagram:**

### Final System Diagram Explained
![End Artifact Diagram](Pictures/System_Diagram_NEW.png)

## **How Did We Fabricate and BOM (Build of Materials):**
### Parts List:
1. Lamp
- EcoClassic Philips:
    - 53 W
    - 220-240 V 
    - Dimmable: Yes
    - Technology: Halogen
    - Colour Temperature (Kelvin): 2800K - Extra Warm White
2. Fan
- Nidec BETA V:
   - 12 V
   - 0.28 A
   - Model: CFA1212025MS
3. Aluminium Foil
4. Box

### Carbonated Drink Recipe
1. Add Baking Soda 
2. Add Dehydrated Fruit Powder(s) (Instead of Sugar)
3. Add Lemon Juice
4. Add Microgreens

# **Listed Future Development Opportunities For This Project**

![Future Food Today Cookbook](Pictures/Future_Food_Today.PNG)

Future Developments:
 * Create New Favorite Foods & Drinks Using Microgreens (Future Food Today Cookbook):
   * Juice
   * Coffee
   * Protein Powder Mix
   * Smoothie  
   * Energy Drink
   * Gelatine
   * Beer
   * Ice Cream/Popsicle
   * Chocolate
   * Candy
   * Gummy Bears
 * Optimize Microgreens Growth Process (Light Recipe) => LED Grow Light Strip
 * Develop Microgreens Post-Harvest Storage Technology => Refrigeration; Controlled Atmosphere Storage (CAS);  Modified Atmosphere Packaging (MAP) 
 * **Optimize Planetary Soda Recipe**

 Collaboration Opportunities:
 * InstaGreen
 * FabLab BCN
 * Space10

 ### LED Light Strip
![LED Light Strip](Pictures/LED_Light_UPDATED.png)

 ### Portable Farm Concept 1
 ![Portable Farm 1](Pictures/Farm_on_Wheels_1_UPDATED.jpg) 

 ### Portable Farm Concept 2
 ![Portable Farm 2](Pictures/Farm_on_Wheels_2_UPDATED.jpg)

 ### Vending Machine Cafe Concept
 ![Vending Machine Cafe](Pictures/Automated_Shop_Concept.PNG) 

 ### References/Bibliography:
 [Space 10 Projects](https://space10.com/projects/)

 [Future Food Today Cookbook](https://space10.com/project/future-food-today/)

 [Spaces on Wheels](https://space10.com/project/spaces-on-wheels-exploring-a-driverless-future/)

 [Controlled Atmosphere Storage](https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/controlled-atmosphere-storage)
 
 [Modified Atmosphere Packaging](https://www.interempresas.net/Envase/Articulos/44932-Envases-plasticos-en-el-envasado-en-atmosfera-modificada.html)

## **Iteration Process: Problems & How We Solved It:**

Problem #1: Dehydration Chamber => Hook Up Together Halogen Lamp (Heat) & Fan (Ventilation/Air Flow)

Problem #2: Fruit Sugar => Dehydrate, Crush & Sift Lemon Peels 

Problem #3: Carbonation Process => Mixing Lemon Juice with Baking Soda 

## **References/Bibliography:**
[Malnutrition: It's More Than Hunger](https://www.who.int/news-room/commentaries/detail/malnutrition-it-s-about-more-than-hunger)

[Malnutrition is a World Health Crisis](https://www.who.int/news/item/13-07-2020-as-more-go-hungry-and-malnutrition-persists-achieving-zero-hunger-by-2030-in-doubt-un-report-warns)

[Zero Hunger 2030 in Doubt](https://www.who.int/news/item/13-07-2020-as-more-go-hungry-and-malnutrition-persists-achieving-zero-hunger-by-2030-in-doubt-un-report-warns)

[Food Security and Nutrition in the World 2020](https://www.who.int/news-room/commentaries/detail/malnutrition-it-s-about-more-than-hunger)

[WHO Urges Government to Promote Healthy Food in Public Facilities](https://www.who.int/news/item/12-01-2021-who-urges-governments-to-promote-healthy-food-in-public-facilities)

[Strategy and Vision For FAOs Work in Nutrition](http://www.fao.org/3/i4185e/i4185e.pdf)

[Maintaining A Healthy Diet During COVID](http://www.fao.org/3/ca8380en/CA8380EN.pdf)

[Global Nutrition Report 2020](https://globalnutritionreport.org/reports/2020-global-nutrition-report/)

[Global Nutrition Report 2018](https://globalnutritionreport.org/reports/global-nutrition-report-2018/executive-summary/)


## **End Artifacts Photographs:**

![End Artifact 1](Pictures/End_Artifact_1.jpg)

![End Artifact 2](Pictures/End_Artifact_2.jpg)

## **Link to Website:**
https://wordpress.com/page/sassyduckweedfarm.wordpress.com/5






